document.getElementById("signupForm").addEventListener("submit", function (event) {
    var password = document.getElementById("password").value;
    var confirmPassword = document.getElementById("confirm-password").value;
    var passwordError = document.getElementById("passwordError");
    var strengthError = document.getElementById("strengthError");
    var successMessage = document.getElementById("successMessage");

    passwordError.style.display = "none";
    strengthError.style.display = "none";
    successMessage.style.display = "none";

    var passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;

    if (password !== confirmPassword) {
        passwordError.style.display = "block";
        event.preventDefault();
        return;
    }

    if (!passwordRegex.test(password)) {
        strengthError.style.display = "block";
        event.preventDefault();
        return;
    }

    successMessage.style.display = "block";

    event.preventDefault();
});

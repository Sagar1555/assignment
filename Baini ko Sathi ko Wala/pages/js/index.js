document.addEventListener('DOMContentLoaded', function () {
    
    var myCarousel = document.querySelector('#carouselExampleSlidesOnly');
    var carousel = new bootstrap.Carousel(myCarousel, {
        interval: 2000,
        wrap: true
    });

    document.querySelectorAll('nav a').forEach(anchor => {
        anchor.addEventListener('click', function (e) {
            e.preventDefault();
            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth'
            });
        });
    });

    var backToTopBtn = document.getElementById('backToTopBtn');
    window.onscroll = function () {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            backToTopBtn.style.display = 'block';
        } else {
            backToTopBtn.style.display = 'none';
        }
    };
    backToTopBtn.addEventListener('click', function () {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    });

  
    var reservationButton = document.getElementById('reservationButton');
    reservationButton.addEventListener('click', function () {
        var reservationModal = new bootstrap.Modal(document.getElementById('reservationModal'));
        reservationModal.show();
    });

    var reservationForm = document.getElementById('reservationForm');
    reservationForm.addEventListener('submit', function (e) {
        e.preventDefault();
        alert('Reservation made successfully!');
        var reservationModal = bootstrap.Modal.getInstance(document.getElementById('reservationModal'));
        reservationModal.hide();
    });
});

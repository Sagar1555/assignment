document.addEventListener('DOMContentLoaded', () => {
    const newsContainer = document.querySelector('.news-items');

    const newsItems = [
        { title: "समुदायिक बैठक", date: "२० मे, २०२५ ", description: "हाम्रो मासिक सामुदायिक बैठकमा सहभागी हुनुहोस्।" },
        { title: "स्वास्थ्य शिविर", date: "१५ जुन, २०२५ ", description: "सबै बासिन्दाहरूको लागि निःशुल्क स्वास्थ्य शिविर।" },
        { title: "शैक्षिक पहल", date: "५ जुलाई, २०२५ ", description: "बालबालिकाहरूको लागि नयाँ शैक्षिक कार्यक्रमहरू।" },
    ];

    newsItems.forEach(item => {
        const newsDiv = document.createElement('div');
        newsDiv.classList.add('news-item');
        newsDiv.innerHTML = `
            <h3>${item.title}</h3>
            <p><small>${item.date}</small></p>
            <p>${item.description}</p>
        `;
        newsContainer.appendChild(newsDiv);
    });
});
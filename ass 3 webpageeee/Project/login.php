<!-- login.php -->
<?php
session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $username = $_POST['username'];
    $password = $_POST['password'];

    
    $valid_username = 'example_user';
    $valid_password = 'example_password';

    if ($username === $valid_username && $password === $valid_password) {
     
        $_SESSION['username'] = $username;
        header('Location: dashboard.php');
        exit;
    } else {
     
        header('Location: login.html?error=1');
        exit;
    }
}
?>
